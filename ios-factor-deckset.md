autoscale: true

# [fit] iOS Factor
---
![](images/felix_twitter_page.png)

- Felix Krause (Twitter & Google)
- Fastlane tools

---
![left fit](images/stackof-iphones.jpeg)
# Reflection: Peak iPhone?

---
![](images/iphone-models-ascension.jpg)
### Over the past 10 years, the iOS development process has shifted drastically:

---
[.build-lists: true]
|FROM|TO|
|-----------|-----------|
| supporting a single device | wide range of available iOS-powered iPhones and iPads and various platforms like tvOS and watchOS|
| manually including git submodules | dependency managers |
| running locally on-device | heavily relying on backend services |
| AppStore review times of more than 2 weeks | less than a day |
| installing an app on your phone using iTunes | distribution platforms e.g. TestFlight |
| uploading 5 screenshots per language | 110 per language (as well as app previews) |
| Objective-C/C++ | a mix of languages including JavaScript |
| slow releases whenever something is ready | shipping every single week |
| Big-Bang releases | A/B testing, slow rollouts and automatic regression detection |

--- 
![right original 200%](images/thinking-face_1f914.png)
# What is The iOS Factor?

---
- experimental project started in May 2018
- Technical Manifesto
- collection of best practices
- based on "The 12 Factor App"
- (a methodology to write high-quality web services)
- bigger-picture iOS app development processes 
- doesn't cover language specific challenges
- encapsulating best practices in an open source way

^ use as guidance to highlight areas of our own project that need improvements

---
[.text: alignment(center)]
**Dependencies**
Explicitly declare and isolate dependencies

**Config**
No configuration in code, ship with default config & allow OTA updates

**Dev/prod parity**
Keep development, testing and production as similar as possible

**Deployment**
Automate your deployment so you can release from any machine

---
[.text: alignment(center)]
**Prefer local over remote**
Keep the iOS app smart enough to operate without a backend where possible

**Backwards compatible APIs**
Don't assume every user updates to the latest version

**App versioning**
Automate your app's build & version numbers for consistency

**Persistence of data**
Follow the Apple guidelines when storing data


---
![right original 200%](images/thinking-face_1f914.png)

[.text: alignment(center)]
## A nudge towards Continuous Delivery & Continuous Integration?

---

# Dependencies

---
**Ideally, your build tools never rely on the implicit existence of system-wide packages.** 
Declare all dependencies including exact versions of Xcode, CocoaPods, Gems, Bundler, Fastlane, Carthage etc. in config files

---
# benefits
- Simplifies setup for developers new to the app
- A reliable build system able to run historical builds in a reproducible fashion

^ a new developer can check out the app’s codebase onto their development machine, requiring only the language runtime and dependency manager installed as prerequisites. Run with a single, simple command

---
## Clean Containers
- reproducible builds

**By specifying the exact dependencies you can re-trigger a build from 6 months ago, knowing that it will succeed** as it will use the same version of Xcode, CocoaPods and Swift.

---
- Web development has this sewn up
- iOS development cannot be containerized (due to needing MacOS to build)
- ([rdar://40669395](https://openradar.appspot.com/radar?id=4929082424819712))
- Limited to third party tools

# :thumbsdown:

---
# Xcode-select

- Of course we can dictate which version of Xcode we use to build
- CI services such as Bitrise & Veertu also make this especially easy
- Still not containerized i.e. cannot pull container and run locally
- Again we can achieve this using third party stuff

---
# Python-based tooling

Python is a language we can use for scripting even within Xcode project build phases

- Dependencies installed with pip
- Python version used inside a virtualenv managed with pipenv
- Config with Pipfile
- Easy installation with homebrew 

```
[[source]]
url = "https://pypi.org/simple"
verify_ssl = true
name = "pypi"
[packages]
xunique = "*"
[dev-packages]
[requires]
python_version = "2.7"
```

^ already got a solid framework just with python

---
# Ruby-based tooling

- Bundler to install Ruby
- Gemfile to install Ruby based dependencies

```ruby
source "https://rubygems.org"

gem "fastlane", ">= 2.96.1", "<= 3.0.0"
gem "cocoapods", "~> 1.5"
```

The `Gemfile` and the automatically generated `Gemfile.lock` must be checked into version control.

---
# JavaScript-based tooling

JavaScript based iOS apps (e.g. React Native) make use of a `package.json` file that defines all dependencies needed.

```json
{
  ...
  "scripts": {
    "start": "node node_modules/react-native/local-cli/cli.js start",
    "test": "jest"
  },
  "dependencies": {
    "react": "15.4.2",
    "react-native": "0.42.3",
    ...
  }
}
```

The `package.json` should be checked into version control for reproducible builds.

---
# Specifying an Xcode version

- Use `.xcode-version` in root of your iOS project to declare the exact version of Xcode to be used for a given iOS project.
- Switch the Xcode version (assuming you already have it installed) using a tool like `chxcode`
- Automate installation of Xcode using the third party tool xcode-install
- (until Apple provides a command line tool to install Xcode (rdar://40669425)

---
# Carthage vs. Cocoapods

The debate rages on.

**(not really)**

---
# Cocoapods
**pros**
- Easy to setup
- Strong community
- Ideal for small projects (where build time is not an issue)

**cons**
- Tight integration into project (intrusive)
- Need to keep an eye on versions of everything (podlock etc.)
- Centralized

---
# Carthage
**pros**
- Flexible
- Written in swift (Carthage Kit)
- Build dependencies once
- Easier to remove (unintrusive)
- Minimal integration
- Decentralized

**cons**
- Manual setup
- New & still small community

---
# Swift Package Manager (SPM)

- The future
- Not fully functioning in Xcode or for iOS

Apple provides an excellent guide on the [Swift Package Manager](https://swift.org/package-manager) building for MacOS.

---
# Config

---
An app’s config is everything that is likely to change between deploys (App Store, TestFlight, local development) or over time without the underlying code changing. This includes:

- API keys for backend services (internal as well as external services)
- URLs for remote resources (the APIs your app uses)
- Feature toggles

---
[.text: alignment(center)]

Avoid storing any kind of config as constants in the codebase

---
![](images/litmusTest.jpg)


A litmus test for whether an app has all config correctly factored out of the code is whether the codebase could be made open source at any moment, without compromising any credentials.

---
# injecting config during build time

- Configuration files (e.g. JSON or YAML files)
- [cocoapods-keys](https://github.com/orta/cocoapods-keys) to better hide keys and apply them to your iOS app during build time
- Custom built solution (e.g. using a build phase)
- environment variables

---
As deployments on the iOS platform are significantly slower than server deployments, you might want a way to quickly update config over the air (OTA) to react to issues fast.

---
OTA config updates are powerful and allow you to instantly

- Run A/B tests to enable certain features or UI changes only for a subset of your active users
- Rotate API keys
- Update web hosts or other URLs that have changed
- Remotely disable features or hide buttons


---
- Without OTA updates you have to wait for about a day for app review to accept your app
- Every submission also adds the risk of being rejected

---
At the same time, you might want to be backwards compatible

- Users who can't upgrade to the most recent iOS version might not be able to install any app updates at all 
- Providing certain updates OTA you can keep supporting older versions of your app

---
Some potential approaches when implementing OTA updates of config:

- Implement your own solution
- Proprietary web services like [Firebase remote config](https://firebase.google.com/docs/remote-config/)

---
# Dev/Prod parity

---
[.text: alignment(center)]

development <=> production

---

Historically, there has been substantial gaps between development and production. 

These gaps manifest in three areas:

- **The time gap:** A developer may work on code that takes days, weeks, or even months to go into production
- **The personnel gap:** All iOS developers write code, just one person knows how to deploy the app
- **The tools gap:** Developers may be using a staging server that runs a different version than the production one. Developers might be using a different Xcode release than the one that was used for deployment

---
**An iOS-factor app is designed for [continuous deployment](https://avc.com/2011/02/continuous-deployment/) by keeping the gap between development and production small.** 

---
- **Make the time gap small** a developer may write code and have it deployed just a few days later
- **Make the personnel gap small** developers who wrote code are closely involved in deploying it and watching its behavior in production. Achieve this by completely automating the release process of your iOS app and put the know-how in code declarations instead of documentation.
- **Make the tools gap small** keep development and production environments as similar as possible. 

---

|          |Traditional|iOS-factor|
|----------|---------------------|----------------|
| Time between deploys | Months | Days |
| Code authors vs code deployers | One person knows how to deploy | Deployment is automated, ideally on a server |
| Dev vs. Prod env | Divergent | As similar as possible |

---
# Deployment

---
As described in the [Dependencies](/dependencies) factor, the code repository should include all dependencies needed to build, test and release the iOS app.

---
As soon as your app fulfills that requirement, you can release new app updates from **any** macOS based machine.

---
In an ideal world, to release an app update you would

- spawn up a completely empty, temporary container
- automatically install all dependencies (e.g. Xcode and [CocoaPods](https://cocoapods.org))
- run the deployment script (e.g. [fastlane](https://fastlane.tools))

---
- Xcode has to run on macOS 
- we can't use this approach 
- rdar://40669395
- running macOS in a virtual environment comes with technical as well as legal challenges
- Veertu, a service that allows you to generate virtual macOS environments on Apple hardware

---
In recent years, disposable containers gained popularity for a variety of reasons:

- fully reproducible builds with zero dependencies to the host operating system
- run on any machine (your local computer, or any server in the cloud)
- a clean build with only the most necessary dependencies

---
Right now, the best approach we as iOS developers can take is:

- Automate the installation of Xcode using [xcode-install](https://github.com/krausefx/xcode-install)
- Make use of an [.xcode-version file](https://github.com/fastlane/ci/blob/master/docs/xcode-version.md) to specify the exact Xcode release
- Define all dependencies in configuration files (see [Dependencies](/dependencies) factor)
- Automate the complete deployment process using a deployment tool like [fastlane](https://fastlane.tools)
- Automate code signing (e.g. [codesigning.guide](https://codesigning.guide))
- Deploy often, ideally on a weekly schedule

Many companies use the concept of `Release trains`: a schedule in which a new version of your app gets released. All code that got merged into your main branch (e.g. `master` or `release`) at the time a release train "leaves" will be shipped to the App Store. This approach is implemented by most large iOS apps.

---
# Prefer local over remote

---

- Trend: Dumb Client
- Requires network access
- User Experience :thumbsdown:

^ In the recent years, some development teams started using approaches that require less development work, on the expense of the user's app experience by moving more logic to a remote backend and having the iOS app be a thin client showing the server results. This approach results in user frustration when the app is used in a situation with a less than perfect internet connection (like in a subway, elevator or a spotty WiFi).

---
[.text: alignment(center)]

**An app should do as much of the business logic and calculations on-device as possible**

---
#why?
- Privacy: avoid sending data to a remote server
- Speed: waiting for response requires time and might fail (e.g. spotty WiFi)
- Data usage: monthly data limits
- Scaling: responsibility of scaling backend up
- Battery life: Using mobile data is costly for battery life
- Reliability: Some countries still have unreliable LTE/3G connections

(apps will almost always require some kind of backend service i.e. initial authentication, more complex calculations or storing of content, querying live data etc.)

---
use face ID to login to app and conduct offline mode rather than need to authenticate

---
**Limit the number of tasks that run on the backend to a minimum to enhance the end-user's experience and to protect their privacy.**

---
All parts of the app that don't necessarily **need** an internet connection (e.g. login) should work without any internet connection at all

---
- Startup screen should never wait for the first successful web response, this may cause bad UX for users with a spotty internet connection

---
- If your app requires an internet connection for everything (e.g. social networking app or ride sharing app), your app should still work (in read-only mode) without an internet connection to access historic data (e.g. recent rides, recent direct messages).

---
- Any feature of your app that needs a working internet connection should show a clear error message that the server couldn't be reached.

---
- As WiFi hotspots might require a login or confirmation of some sorts (e.g. hotel or airport), HTTPS requests will often get stuck and time-out after about a minute. Until Apple resolves this issue on a system level, we as developers have to make sure to properly handle those situations.

---
- Never assume a user has a working internet connection on the first launch of the app. The user might install your app and then doesn't open it until they're on the go without an internet connection. You are responsible for shipping your app in a state that it works out of the box with the most up to date resources during deploy time.

---
# Startup screen 

---
[.background-color: #fafafa]
![fit](images/startupscreen.png)

---
# Startup screen 
- the launch screen is quickly replaced with the first screen 
- give the impression that your app is fast and responsive
- the launch screen isn't an opportunity for artistic expression
- intended to enhance the perception of your app as quick to launch

---
[.background-color: #fafafa]
![fit](images/startupscreen_before.png)

---
[.background-color: #fafafa]
![fit](images/startupscreen_after.png)

---


# Backwards compatible APIs

---

While the majority of your end-users will update to the most recent update within a few weeks, there will always be a subset of users who won't. This can have multiple reasons. Often it is related to the iOS version they run, which they can't always update depending on how old the given iOS device is.

---
e.g. You can install and use Facebook Messenger on a first-generation iPad (2010). While newer features are not supported, the core functionality is still available thanks to API versioning.

---
The basic concept is that you don't update an existing API, but add a new one instead and let them run in parallel

```
https://your-api.com/1/drivers.json
https://your-api.com/2/drivers.json
```

You may eventually need to turn off or slightly change semantics of an API. Even if your company has a deep commitment to stability, sometimes matters of law force this change. This means you should encode an API endpoint that is queryable just to indicate the API status.

```
https://your-api.com/1/status.json
https://your-api.com/2/status.json
```

The status API should include information such as

 - What's the stage of the API? (e.g. testing, beta, production)
 - Is the API deprecated? If so, what date is it scheduled to be turned off?
 - Is the API offline? If so, will it remain so or is it a temporary outage?

 NEED AN EXAMPLE HERE

---
# App versioning

---
**Version & Build numbers work together to uniquely identify a particular App Store submission**

- Version number (`CFBundleShortVersionString`) - shown as `Version` in Xcode, also called the marketing version: The version that's visible to the end-user, it has to be incremented on each public App Store release
- Build number (`CFBundleVersion`) - shown as `Build` in Xcode: an incrementing number

---
In today's iOS development processes there is no reason you should manually change those numbers. Instead, you want a reliable and automated system taking care of keeping the versions up to date.

There is no need to use third-party tools, Xcode has a tool called `agvtool` built-in. ([more details](https://developer.apple.com/library/content/qa/qa1827/_index.html))

---
```sh
# Update the version number (CFBundleShortVersionString)
agvtool new-marketing-version 2.0

# Update the build number (CFBundleVersion) to the next one
agvtool next-version -all
```

Example `Fastfile`:

```ruby
lane :beta do
  increment_build_number
  build_ios_app
  testflight
end
```

---

App version number appears to be manual
build version uses the build number from the Jenkins job

---

# Persistence of data

---
[.text: alignment(center)]

Storing data and configuration according to Apple's guideline is crucial particulary when it comes to iCloud sync, upgrading to a new phone and restoring a phone from a backup.

---
## Apples Guidelines: -
- `Documents`: a directory for user-generated content - will be backed up
- `Caches`: a directory for data that can be regenerated - will __not__ be backed up
- `tmp`: Use this directory for temporary files - will __not__ be backed up
- Make use of the `do not back up` attribute for files

^ do not backup is a misleading name. What this means is do not delete or purge as part of the backing up process.  If your device is running low on memory, it will start to purge files that it knows are backed-up on iCloud, hence, setting this property is saying this file is not backedup which indicates to iOS that it shouldn't be deleted as part of this usual process.

---
Never store sensitive user-information (like passwords or sessions) in those directories. Instead use the Keychain API.

---
The Keychain API gives you control of how data is being stored on device. Make sure you have a good understanding of how the [various attributes](https://developer.apple.com/documentation/security/keychain_services/keychain_items/item_attribute_keys_and_values) affect the lifecycle of your app.

---
One often overlooked question you should ask yourself: When the user upgrades to a new iOS device, should the data (e.g. login session) be migrated as well?

If you use [`kSecAttrAccessibleWhenUnlockedThisDeviceOnly`](https://developer.apple.com/documentation/security/ksecattraccessiblewhenunlockedthisdeviceonly) the data won't be included in the iCloud or iTunes backup, meaning the user will lose the data when they upgrade their device.

---

# Like and Subscribe
![inline](images/twitter-bird-white-on-blue.png) 
@Creagh2
![inline](images/youtube-icon.png) 
Daniel Creagh 



<!-- (Ignore the terrorist guy) -->
